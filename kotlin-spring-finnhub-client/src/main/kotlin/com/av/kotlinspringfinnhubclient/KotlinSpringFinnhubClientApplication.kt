package com.av.kotlinspringfinnhubclient

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinSpringFinnhubClientApplication

fun main(args: Array<String>) {
	runApplication<KotlinSpringFinnhubClientApplication>(*args)
	System.exit(0);
}
