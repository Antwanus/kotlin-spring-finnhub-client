package com.av.kotlinspringfinnhubclient.controller.v1

import com.av.kotlinspringfinnhubclient.service.FinnhubService
import com.av.kotlinspringfinnhubclient.service.QuoteService

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/v1")
class QuoteController {
    @Autowired  lateinit var quoteService: QuoteService
    @Autowired  lateinit var finnhubService: FinnhubService

    @GetMapping("/quotes")
    fun getAllQuotes() = quoteService.findAllSavedQuotes()

    @GetMapping("/quote/{symbol}")
    fun getQuoteForSymbol(@PathVariable symbol: String) = finnhubService.findQuoteForSymbol(symbol)

}