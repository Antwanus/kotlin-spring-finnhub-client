package com.av.kotlinspringfinnhubclient.domain;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Order {
   private String status;
   private String date;
   private String name;
   private String typeTx;
   private String typeOrder;
   private String typeEffect;
   private int aantal;
   private double koers;
   private double totaal;
   private double limietKoers;
   private String trigger;
   private String geldigTot;
   private String referentie;

   public Order() {}

   public double calculateProfit() {
      if (isOrderGeannuleerd())
         return 0.0;
      else
         return formatPrice(this.aantal * calculateProfitPerShare());
   }
   public double calculateProfitPerShare() {
      if (isOrderGeannuleerd())
         return 0.0;
      else
         return formatPrice(this.koers - calculateAverageBuyPrice());
   }
   public double calculateAverageBuyPrice() {
      if (isOrderGeannuleerd())
         return 0.0;
      else
         return formatPrice(this.totaal / this.aantal);
   }
   private boolean isOrderGeannuleerd() {
      return this.status.equalsIgnoreCase("geannuleerd");
   }
   public double formatPrice(double price) {
      var df = new DecimalFormat("#.###");
      df.setRoundingMode(RoundingMode.CEILING);
      return Double.parseDouble(df.format(price));
   }
   @Override
   public String toString() {
      return "Order{" +
          "status='" + status + '\'' +
          ", date='" + date + '\'' +
          ", name='" + name + '\'' +
          ", typeTx='" + typeTx + '\'' +
          ", typeOrder='" + typeOrder + '\'' +
          ", typeEffect='" + typeEffect + '\'' +
          ", aantal=" + aantal +
          ", koers=" + koers +
          ", totaal=" + totaal +
          ", limietKoers=" + limietKoers +
          ", trigger='" + trigger + '\'' +
          ", geldigTot='" + geldigTot + '\'' +
          ", referentie='" + referentie + '\'' +
          ", rendement='" + calculateProfit() + '\'' +
          '}';
   }

   public String getStatus() { return status; }
   public void setStatus(String status) { this.status = status; }
   public String getDate() { return date; }
   public void setDate(String date) { this.date = date; }
   public String getName() { return name; }
   public void setName(String name) { this.name = name; }
   public String getTypeTx() { return typeTx; }
   public void setTypeTx(String typeTx) { this.typeTx = typeTx; }
   public String getTypeOrder() { return typeOrder; }
   public void setTypeOrder(String typeOrder) { this.typeOrder = typeOrder; }
   public String getTypeEffect() { return typeEffect; }
   public void setTypeEffect(String typeEffect) { this.typeEffect = typeEffect; }
   public int getAantal() { return aantal; }
   public void setAantal(int aantal) { this.aantal = aantal; }
   public double getKoers() { return koers; }
   public void setKoers(double koers) { this.koers = koers; }
   public double getTotaal() { return totaal; }
   public void setTotaal(double totaal) { this.totaal = totaal; }
   public double getLimietKoers() { return limietKoers; }
   public void setLimietKoers(double limietKoers) { this.limietKoers = limietKoers; }
   public String getTrigger() { return trigger; }
   public void setTrigger(String trigger) { this.trigger = trigger; }
   public String getGeldigTot() { return geldigTot; }
   public void setGeldigTot(String geldigTot) { this.geldigTot = geldigTot; }
   public String getReferentie() { return referentie; }
   public void setReferentie(String referentie) { this.referentie = referentie; }
}
