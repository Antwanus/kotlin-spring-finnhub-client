package com.av.kotlinspringfinnhubclient.domain

import java.sql.Timestamp
import javax.persistence.*

@Entity
data class Quote (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id:Long,
    val o:Double,       // openPriceOfTheDay
    val h:Double,       // highPriceOfTheDay
    val l:Double,       // lowPriceOfTheDay
    val c:Double,       // currentPrice
    val pc:Double,      // previousClosedPrice
    val t:Long          // timestamp
) {
    @ManyToOne
    lateinit var symbol:Symbol

    override fun toString():String = "Quote ($t): previous closed price = $pc"
    fun getTimestamp():Timestamp = Timestamp(t)
}
