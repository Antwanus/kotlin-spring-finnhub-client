package com.av.kotlinspringfinnhubclient.domain

import javax.persistence.*

@Entity
data class Symbol (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id:Long,
    val name:String
) {
    @OneToMany(mappedBy = "symbol")
    lateinit var quotes:List<Quote>
}
