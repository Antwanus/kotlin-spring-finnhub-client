package com.av.kotlinspringfinnhubclient.repo

import com.av.kotlinspringfinnhubclient.domain.Quote
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface QuoteRepo:CrudRepository<Quote, Long> {
    fun findQuotesBySymbolName(symbol: String):List<Quote>
}