package com.av.kotlinspringfinnhubclient.repo

import com.av.kotlinspringfinnhubclient.domain.Symbol
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface SymbolRepo:CrudRepository<Symbol, Long> { }