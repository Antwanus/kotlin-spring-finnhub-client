package com.av.kotlinspringfinnhubclient.service;

import com.av.kotlinspringfinnhubclient.domain.Order;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CSVService {
   public static void main(String[] args) {
      readOrdersFromCSV("src/main/resources/csv/order.csv")
          .forEach(System.out::println);
   }

   private static List<Order> readOrdersFromCSV(String filename) {
      List<Order> orders = new ArrayList<>();
      var pathToFile = Paths.get(filename);

      try (var reader = Files.newBufferedReader(pathToFile, StandardCharsets.UTF_8)) {
         String line = reader.readLine();
         var sumTotal = 0;
         while (line != null) {
            String[] attributes = line.split(",");
            var order = new Order();
            order.setStatus(attributes[0]);
            order.setDate(attributes[1]);
            order.setName(attributes[2]);
            order.setTypeTx(attributes[3]);
            order.setTypeOrder(attributes[4]);
            order.setTypeEffect(attributes[5]);
            order.setAantal(Integer.parseInt(attributes[6]));
            var tmp = attributes[7].replace(" EUR", "");
            order.setKoers(Double.parseDouble(tmp));
            tmp = attributes[8].replace(" EUR", "");
            order.setTotaal(Double.parseDouble(tmp));
            tmp = attributes[9].replace(" EUR", "");
            order.setLimietKoers(Double.parseDouble(tmp));
            order.setTrigger(attributes[10]);
            order.setGeldigTot(attributes[11]);
            order.setReferentie(attributes[12]);
            orders.add(order);
            line = reader.readLine();

            System.out.println("\t" + order.getName() + " (" + order.getDate() + ")");
            System.out.println("Amount of shares : " + order.getAantal());
            System.out.println("Average buy price: " + order.calculateAverageBuyPrice());
            System.out.println("Profit for order : " + order.calculateProfit());

            sumTotal += order.calculateProfit();
         }
         System.out.println("Total profit : " + sumTotal);

      } catch (IOException e) {
         e.printStackTrace();
      }

      return orders.stream()
          .filter(order -> order.getStatus().equalsIgnoreCase("uitgevoerd"))
          .collect(Collectors.toList());
   }
}
