package com.av.kotlinspringfinnhubclient.service

import com.av.kotlinspringfinnhubclient.domain.Quote
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

@Service
class FinnhubService {
    val webclient = WebClient.builder().build()

    fun findQuoteForSymbol(symbol: String): Mono<Quote> {
        return webclient.get()
            .uri("https://finnhub.io/api/v1/quote?symbol=$symbol")
            .header("X-Finnhub-Token", "c1rddr2ad3iautolma5g")
            .retrieve()
            .bodyToMono(Quote::class.java)

    }
}