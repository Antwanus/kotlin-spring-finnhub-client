package com.av.kotlinspringfinnhubclient.service

import com.av.kotlinspringfinnhubclient.domain.Quote


interface QuoteService {
    fun findAllSavedQuotes():List<Quote>


}