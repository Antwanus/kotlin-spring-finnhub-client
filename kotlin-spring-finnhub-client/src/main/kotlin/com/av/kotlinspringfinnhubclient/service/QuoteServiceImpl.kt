package com.av.kotlinspringfinnhubclient.service

import com.av.kotlinspringfinnhubclient.domain.Quote
import com.av.kotlinspringfinnhubclient.repo.QuoteRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class QuoteServiceImpl : QuoteService {
    @Autowired
    lateinit var quoteRepo:QuoteRepo

    override fun findAllSavedQuotes(): List<Quote> {
        return quoteRepo.findAll().toList()
    }

    fun getQuotesBySymbol(symbol: String): List<Quote> {
        return quoteRepo.findQuotesBySymbolName(symbol)
    }
}